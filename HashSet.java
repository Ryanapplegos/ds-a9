// Morgan Hobson - mhobson5@jhu.edu
// Xingyi Yang   - xyang73@jhu.edu
// Data Structures - Assignment 9
// 05/01/2019

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.reflect.Array;

/**
 * Set implemented using plain Java arrays with hashed positions.
 *
 * @param <T> Element type.
 */
public class HashSet<T> implements Set<T> {
    private int used; // contiguous rep, no gaps allowed
    private Entry[] data; // no duplicates in rep, see insert()
    private int version;

    private class Entry {
        T t;

        Entry(T t) {
            this.t = t;
        }

        public void kill() {
            this.t = null;
        }

        public boolean dead() {
            return this.t == null;
        }
    }

    /**
     * Create an empty set.
     */
    public HashSet() {
        this.data = (Entry[]) Array.newInstance(Entry.class, 8);
    }

    private int hashPos(T t) {
        return (t.hashCode() & 0X7FFFFFFF) & (this.data.length - 1);
    }

    private boolean full() {
        boolean boo;
        if (((double) this.used) / this.data.length > 0.9) {
            boo = true;
        } else {
            boo = false;
        }
        return boo;
    }

    private void grow() {
        Entry[] temp = this.data;
        this.data = (Entry[]) Array.newInstance(Entry.class, temp.length * 2);
        for (int i = 0; i < temp.length; i++) {
            Entry e = temp[i];
            if (e != null && !e.dead()) {
                int pos = this.hashPos(e.t);
                //linear probing
                while (this.data[pos] != null) {
                    pos = (pos + 1) & (this.data.length - 1);
                }
                this.data[pos] = e;
            }
        }
    }

    private int find(T t) {
        int pos = this.hashPos(t);
        while (this.data[pos] != null) {
            if (!this.data[pos].dead() && this.data[pos].t.equals(t)) {
                return pos;
            }
            pos = (pos + 1) & (this.data.length - 1);
        }
        return -1;
    }

    @Override
    public void insert(T t) {
        if (this.has(t)) {
            return;
        }

        int pos = this.hashPos(t);
        while (this.data[pos] != null) {
            pos = (pos + 1) & (this.data.length - 1);
        }

        this.data[pos] = new Entry(t);
        this.used++;
        this.version++;
        if (this.full()) {
            this.grow();
        }
    }

    @Override
    public void remove(T t) {
        int p = this.find(t);
        if (p == -1) {
            return;
        }
        this.data[p].kill();
        this.used--;
        this.version++;
    }

    @Override
    public boolean has(T t) {
        return this.find(t) != -1;
    }

    @Override
    public int size() {
        return this.used;
    }

    @Override
    public Iterator<T> iterator() {
        return new SetIterator();
    }

    private class SetIterator implements Iterator<T> {
        int current;
        int passed;
        int version;

        SetIterator() {
            this.version = HashSet.this.version;
        }

        private void checkVersion() {
            if (this.version != HashSet.this.version) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public boolean hasNext() {
            this.checkVersion();
            return this.passed < HashSet.this.used;
        }

        @Override
        public T next() throws NoSuchElementException {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            while (HashSet.this.data[this.current] == null
                    || HashSet.this.data[this.current].dead()) {
                this.current++;
            }
            T t = HashSet.this.data[this.current].t;
            this.current++;
            this.passed++;
            return t;
        }
    }
}
