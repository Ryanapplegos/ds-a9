// Morgan Hobson - mhobson5@jhu.edu
// Xingyi Yabg   - xyang73@jhu.edu
// Data Structures - Assignment 9
// 05/01/2019

import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class HashSetTest extends SetTestBase {
    protected Set<String> createSet() {
		return new HashSet<String>();
	}
}
