// Morgan Hobson - mhobson5@jhu.edu
// Xingyi Yabg   - xyang73@jhu.edu
// Data Structures - Assignment 9
// 05/01/2019

import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public abstract class SetTestBase {
    protected Set<String> set;
	protected static int SIZE = 100;

    protected abstract Set<String> createSet();

    @Before
    public void setupEmptySet() {
        this.set = this.createSet();
    }

    protected void fillSetLinear() {
		for (int i = 0; i < SIZE; i++) {
			this.set.insert(Integer.toString(i));
		}
    }

    @Test
    public void emptySetSize() {
		assertEquals(this.set.size(), 0);
    }

	@Test
	public void insertAffectsSize() {
		for (int i = 1; i < SIZE; i++) {
			this.set.insert(Integer.toString(i));
			assertEquals(this.set.size(), i);
		}
	}

	@Test
	public void removeAffectsSize() {
		this.fillSetLinear();
		for (int i = SIZE - 1; i >= 0; i--) {
			this.set.remove(Integer.toString(i));
			assertEquals(this.set.size(), i);
		}
	}

	@Test
	public void insertMultipleTimes() {
		for (int i = 0; i < SIZE; i++) {
			this.fillSetLinear();
			assertEquals(this.set.size(), SIZE);
		}
	}

	@Test
	public void removeMultipleTimesSize() {
		this.fillSetLinear();
		for (int i = SIZE - 1; i >= 0; i--) {
			this.set.remove(Integer.toString(i));
			this.set.remove(Integer.toString(i));
			assertEquals(this.set.size(), i);
		}
	}

	@Test
	public void removeMultipleTimesHasNot() {
		this.fillSetLinear();
		for (int i = 0; i < SIZE; i++) {
			String s = Integer.toString(i);
			this.set.remove(s);
			this.set.remove(s);
			assertFalse(this.set.has(s));
		}
	}

	@Test
	public void insertHas() {
		this.fillSetLinear();
		for (int i = 0; i < SIZE; i++) {
			assertTrue(this.set.has(Integer.toString(i)));
		}
	}

	@Test
	public void removeHasNot() {
		this.fillSetLinear();
		for (int i = 0; i < SIZE; i++) {
			String s = Integer.toString(i);
			this.set.remove(s);
			assertFalse(this.set.has(s));
		}
	}
}
