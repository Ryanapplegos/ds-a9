// Morgan Hobson - mhobson5@jhu.edu
// Xingyi Yabg   - xyang73@jhu.edu
// Data Structures - Assignment 9
// 05/01/2019

import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public abstract class MapTestBase {
    protected final int SIZE = 100;
    protected Map<Integer, Integer> map;

    protected abstract Map<Integer, Integer> createMap();

    @Before
    public void setupMap() {
        map = createMap();
    }

    protected void insertLinear() {
        for (int i = 0; i < SIZE; i++) {
            map.insert(i, i);
        }
    }

    protected void insertReverse() {
        for (int i = SIZE - 1; i >= 0; i--) {
            map.insert(i, i);
        }
    }

    protected void insertMixed() {
        for (int i = 0; i < SIZE/2; i++) {
            map.insert(i, i);
            map.insert(SIZE - i - 1, SIZE - i - 1);
        }
    }

    protected void removeLinear() {
        for (int i = 0; i < SIZE; i++) {
            map.remove(i);
        }
    }

    protected void removeReverse() {
        for (int i = SIZE - 1; i >= 0; i--) {
            map.remove(i);
        }
    }

    protected void removeMixed() {
        for (int i = SIZE/2 - 1; i >= 0; i--) {
            map.remove(i);
            map.remove(SIZE - i - 1);
        }
    }

    @Test
    public void newArraySize() {
        assertEquals(new Integer(0), new Integer(map.size()));
    }

    @Test
    public void keysMatchValues() {
        this.insertLinear();
        for (Integer n: map) {
            assertEquals(n, map.get(n));
        }
    }

    @Test
    public void putChangedValues() {
        this.insertLinear();
        for (Integer n: map) {
            map.put(n, 0);
            assertEquals(map.get(n), new Integer(0));
        }
    }

    @Test
    public void iteratorCount() {
        this.insertReverse();
        int i = 0;
        for (Integer n: map) {
            i++;
        }
        assertEquals(new Integer(i), new Integer(map.size()));
    }

    @Test
    public void hasValues() {
        this.insertLinear();
        for (Integer n: map) {
            assertTrue(map.has(map.get(n)));
        }
    }

    @Test
    public void hasNotValues() {
        this.insertLinear();
        for (Integer n: map) {
            assertFalse(map.has(map.get(n) + SIZE));
        }
    }

    @Test
    public void appropriateSize() {
        for (int i = 0; i < SIZE; i++) {
            assertEquals(new Integer(i), new Integer(map.size()));
            map.insert(i, i);
        }
    }

    @Test
    public void insertReverseHas() {
        this.insertReverse();
        for (int i = 0; i < SIZE; i++) {
            assertTrue(map.has(i));
        }
    }

    @Test
    public void insertMixedHas() {
        this.insertMixed();
        for (int i = 0; i < SIZE; i++) {
            assertTrue(map.has(i));
        }
    }

    @Test
    public void removeLinearNotHas() {
        this.insertLinear();
        this.removeLinear();
        for (int i = 0; i < SIZE; i++) {
            assertFalse(map.has(i));
        }
    }

    @Test
    public void removeReverseNotHas() {
        this.insertLinear();
        this.removeReverse();
        for (int i = 0; i < SIZE; i++) {
            assertFalse(map.has(i));
        }
    }

    @Test
    public void removeMixedNotHas() {
        this.insertLinear();
        this.removeMixed();
        for (int i = 0; i < SIZE; i++) {
            assertFalse(map.has(i));
        }
    }

    @Test
    public void removeLinearSize() {
        this.insertLinear();
        this.removeLinear();
        assertEquals(new Integer(0), new Integer(map.size()));
    }

    @Test
    public void removeReverseSize() {
        this.insertLinear();
        this.removeReverse();
        assertEquals(new Integer(0), new Integer(map.size()));
    }

    @Test
    public void removeMixedSize() {
        this.insertLinear();
        this.removeMixed();
        assertEquals(new Integer(0), new Integer(map.size()));
    }

    @Test(expected=IllegalArgumentException.class)
    public void insertNull() {
        map.insert(null, 1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void insertRepeat() {
        map.insert(1, 1);
        map.insert(1, 2);
    }

    @Test(expected=IllegalArgumentException.class)
    public void removeNull() {
        map.remove(null);
    }

    @Test(expected=IllegalArgumentException.class)
    public void removeNotHas() {
        map.remove(1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void putNull() {
        map.put(null, 1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void putNotHas() {
        map.put(1, 1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void getNull() {
        map.get(null);
    }

    @Test(expected=IllegalArgumentException.class)
    public void getNotHas() {
        map.get(1);
    }
}
