// Morgan Hobson - mhobson5@jhu.edu
// Xingyi Yang   - xyang73@jhu.edu
// Data Structures - Assignment 9
// 05/01/2019

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.reflect.Array;

/** Implementation of Map interface using hashing.
 *
 * @param <K> Key value type
 * @param <V> Mapped value type
 */
public class HashMap<K, V> implements Map<K, V> {

    private class Entry {
        K key;
        V value;

        Entry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public void kill() {
            this.key = null;
            this.value = null;
        }

        public boolean dead() {
            return this.key == null;
        }
    }

    private class HashMapIterator implements Iterator<K> {
        private int returned;
        private int pos;
        //private Iterator<Entry> itt;

        public boolean hasNext() {
            return this.returned < HashMap.this.used;
        }

        public K next() throws NoSuchElementException {
            if (this.hasNext()) {
                while (HashMap.this.data[this.pos] == null
                        || HashMap.this.data[this.pos].dead()) {
                    this.pos = (this.pos + 1) & (HashMap.this.data.length - 1);
                }
                this.returned++;
                K key = HashMap.this.data[this.pos].key;
                this.pos = (this.pos + 1) & (HashMap.this.data.length - 1);
                return key;
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }

    private static final int INITIAL_SIZE = 8;
    private Entry[] data;
    private int used;

    /**Default HashMap constructor.
     */
    public HashMap() {
        this.data = (Entry[]) Array.newInstance(Entry.class, INITIAL_SIZE);
    }

    private int hashPos(K k) {
        return (k.hashCode() & 0X7FFFFFFF) & (this.data.length - 1);
    }

    private void expand() {
        Entry[] temp = this.data;
        this.data = (Entry[]) Array.newInstance(Entry.class, temp.length * 2);
        for (int i = 0; i < temp.length; i++) {
            Entry e = temp[i];
            if (e != null && !e.dead()) {
                int pos = this.hashPos(e.key);
                //linear probing
                while (this.data[pos] != null) {
                    pos = (pos + 1) & (this.data.length - 1);
                }
                this.data[pos] = e;
            }
        }
    }

    private int find(K k) {
        return this.findFromPos(k, this.hashPos(k));
    }

    //assumes pos as starting index for search
    private int findFromPos(K k, int pos) {
        //linear probing
        while (this.data[pos] != null) {
            if (!this.data[pos].dead() && this.data[pos].key.equals(k)) {
                return pos;
            }
            pos = (pos + 1) & (this.data.length - 1);
        }
        return -1;
    }

    private boolean hasFromPos(K k, int pos) {
        pos = this.findFromPos(k, pos);
        if (pos != -1) {
            return true;
        }
        return false;
    }

    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int pos = this.hashPos(k);
        if (this.hasFromPos(k, pos)) {
            throw new IllegalArgumentException();
        }
        while (this.data[pos] != null) {
            if (this.data[pos].dead()) {
                break;
            }
            pos = (pos + 1) & (this.data.length - 1);
        }
        this.data[pos] = new Entry(k, v);
        this.used++;
        if (((double) this.used) / this.data.length > 0.9) {
            this.expand();
        }
    }

    @Override
    public V remove(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int pos = this.find(k);
        if (pos == -1) {
            throw new IllegalArgumentException();
        }
        this.used--;
        V v = this.data[pos].value;
        this.data[pos].kill();
        return v;
    }

    @Override
    public void put(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int pos = this.find(k);
        if (pos == -1) {
            throw new IllegalArgumentException();
        }
        this.data[pos].value = v;
    }

    @Override
    public V get(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int pos = this.find(k);
        if (pos == -1) {
            throw new IllegalArgumentException();
        }
        return this.data[pos].value;
    }

    @Override
    public boolean has(K k) {
        int pos = this.find(k);
        if (pos != -1 && this.data[pos].value != null) {
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return this.used;
    }

    @Override
    public Iterator<K> iterator() {
        return new HashMapIterator();
    }
}
