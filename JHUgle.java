// Morgan Hobson - mhobson5@jhu.edu
// Xingyi Yang   - xyang73@jhu.edu
// Data Structures - Assignment 9
// 05/01/2019

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.util.regex.Pattern;
import java.util.Scanner;
import java.util.Stack;
import java.util.ArrayList;
import java.util.EmptyStackException;

/**
 * the user interface.
 */
public final class JHUgle {
    private static Map<String, ArrayList<String>> hmap;
    private static Stack<ArrayList<String>> st;


    private JHUgle() {}

    //add word-url pairs into the map
    private static void addPairs(String word, String v) {
        ArrayList<String> value;
        if (!hmap.has(word)) {
            value = new ArrayList<>();
            value.add(v);
            hmap.insert(word, value);
        } else {
            value = hmap.get(word);
            value.add(v);
            hmap.put(word, value);
        }
    }

    //parse the input
    private static void readIn(String fname) throws IOException {
        Pattern pattern = Pattern.compile("[\\s[^0-9a-zA-Z]]+");
        FileReader in = new FileReader(fname);

        BufferedReader reader = new BufferedReader(in);

        String line;
        int ind = 0;
        String v = null;

        while ((line = reader.readLine()) != null) {
            if (ind == 0) {
                v = line;
                ind++;
            } else {
                String[] words = pattern.split(line);
                for (String word: words) {
                    if (word.length() <= 1) {
                        // Skip "short" words, most of which just "dirty up"
                        // the statistics.
                        continue;
                    }
                    addPairs(word, v);
                }
                ind--;
            }
        }
        reader.close();
        in.close();
    }

    //helper function for AND and OR operation
    private static void compare(ArrayList<String> s1, ArrayList<String> s2,
        String s) {
        if ("&&".equals(s)) {
            andOperation(s1, s2);
        } else {
            ArrayList<String> orResult = new ArrayList<>();
            HashSet<String> set = new HashSet<>();
            for (String i : s1) {
                set.insert(i);
            }
            for (String i : s2) {
                set.insert(i);
            }
            for (String str : set) {
                orResult.add(str);
            }
            st.push(orResult);
        }
    }

    //helper for AND operation
    private static void andOperation(ArrayList<String> s1,
        ArrayList<String> s2) {
        HashSet<String> set = new HashSet<>();
        if (s1.size() > s2.size()) {
            for (String s : s1) {
                set.insert(s);
            }
            for (String s : s2) {
                if (set.has(s)) {
                    System.out.println(s);
                }
            }
        } else {
            ArrayList<String> andResult = new ArrayList<>();
            for (String s : s2) {
                set.insert(s);
            }
            for (String s : s1) {
                if (set.has(s)) {
                    andResult.add(s);
                }
            }
            st.push(andResult);
        }
    }

    /**
     * This is the main method.
     * @param args Command line arguments.
     * @throws IOException in the unlikely event of a loss of input pressure.
     */
    private static void printOperation() {
        if (st.empty()) {
            System.out.print("> ");
            return;
        }

        ArrayList<String> urls;
        try {
            urls = st.peek();
        } catch (IllegalArgumentException e) {
            System.out.print("> ");
            return;
        }

        for (String string : urls) {
            System.out.println(string);
        }
    }

    //helper for adding urls onto the stack
    private static void putOnStack(String s) {
        try {
            st.push(hmap.get(s));
        } catch (IllegalArgumentException e) {
            System.out.print("> ");
            return;
        }
    }

    /**
     * Main method.
     * @param args Command line arguments (ignored).
     * @throws IOException in the unlikely event of a loss of input pressure.
     */

    public static void main(String[] args) throws IOException {

        //error handling
        if (args.length != 1) {
            return;
        }

        String fname = args[0];
        hmap = new HashMap<>();
        readIn(fname);

        System.out.println("Index Created");

        System.out.print("> ");
        st = new Stack<>();
        //read user input
        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {
            String s = input.next();
            if ("!".equals(s)) {
                System.exit(1);
            } else if ("?".equals(s)) {
                printOperation();
            } else if ("&&".equals(s) || "||".equals(s)) {
                ArrayList<String> url1;
                ArrayList<String> url2;
                try {
                    url1 = st.pop();
                } catch (EmptyStackException e) {
                    System.out.print("> ");
                    continue;
                }
                try {
                    url2 = st.pop();
                } catch (EmptyStackException e) {
                    System.out.print("> ");
                    continue;
                }
                compare(url1, url2, s);
            } else {
                putOnStack(s);
            }
            System.out.print("> ");
        }
        System.out.println("hashset");
    }
}
