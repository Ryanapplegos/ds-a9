import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;
import java.util.Collections;
import java.util.Arrays;


/**
 * Morgan Hobson - mhobson5@jhu.edu
 * Xingyi Yang   - xyang73@jhu.edu
 */

public final class HashMapBench {
    private static final int SIZE = 400;
    private static final Random RAND = new Random();
    private static final String VALUE = "value";
    private static Integer[] arr;


    private HashMapBench() {}

    private static void generateRandomArray() {
        arr = new Integer[SIZE];
        for (int i = 0; i < SIZE; i++) {
            arr[i] = i;
        }
        Collections.shuffle(Arrays.asList(arr)); 
    }

    private static void insertLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert (i, VALUE);
        }
    } 

    private static void insertRandom(Map<Integer, String> m) {
        generateRandomArray();
        for (int i = 0; i < SIZE; i++) {
            m.insert (arr[i], VALUE);
        }
    }

    private static void removeLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.remove (i);
        }
    } 

    private static void removeRandom(Map<Integer, String> m) {
        generateRandomArray();
        for (int i = 0; i < SIZE; i++) {
            m.remove (arr[i]);
        }
    }

    private static void putLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put (i, VALUE);
        }
    } 

    private static void putRandom(Map<Integer, String> m) {
        generateRandomArray();
        for (int i = 0; i < SIZE; i++) {
            m.put (arr[i], VALUE);
        }
    }

    private static void getLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.get (i);
        }
    } 

    private static void getRandom(Map<Integer, String> m) {
        generateRandomArray();
        for (int i = 0; i < SIZE; i++) {
            m.get (arr[i]);
        }
    }

    private static void hasLinear(Map<Integer, String> m) {
        for (int i = 0; i < SIZE; i++) {
            m.has (i);
        }
    } 

    private static void hasRandom(Map<Integer, String> m) {
        generateRandomArray();
        for (int i = 0; i < SIZE; i++) {
            m.has (arr[i]);
        }
    }


    @Bench
    public static void insertLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            b.start();
            insertLinear(m);
        }
    }

    @Bench
    public static void insertRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            b.start();
            insertRandom(m);
        }
    }


    @Bench
    public static void insertLinearRemoveRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertLinear(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public static void insertLinearRemoveLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertLinear(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public static void insertRandomRemoveRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            removeRandom(m);
        }
    }

    @Bench
    public static void insertRandomRemoveLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public static void insertRandomPutRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            putRandom(m);
        }
    }

    @Bench
    public static void InsertRandomGetLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            getLinear(m);
        }
    }


    @Bench
    public static void InsertRandomGetRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            getRandom(m);
        }
    }

    @Bench
    public static void InsertRandomHasLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            hasLinear(m);
        }
    }


    @Bench
    public static void InsertRandomHasRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<Integer, String> m = new HashMap<>();
            insertRandom(m);
            b.start();
            hasRandom(m);
        }
    }

}